import axios from 'axios'
const api = '/api/users'

const getUsers = async () => {
    const response = await axios.get(api)
    return response.data
} 

export default getUsers 
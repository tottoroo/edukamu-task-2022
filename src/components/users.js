import React, { useState, useEffect } from 'react'
import getUsers from '../api.js'
import {
    Box,
    Heading,
    Table,
    TableContainer,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Stack,
    useToast
} from '@chakra-ui/react'

const Users = () => {

    const toast = useToast()
    const infoToast = status => {
        if (status === 0) {
            toast({
                title: "Virhe",
                description: "Tarkasta palvelin",
                status: "error",
                duration: 1500,
                isClosable: true
            })
        }
        else {
            toast({
                title: "OK",
                description: "Käyttäjät ladattu",
                status: "success",
                duration: 1500,
                isClosable: true
            })
        }
    }

    const [users, setUsers] = useState([])

    const fetchData = () => {
        getUsers()
            .then(data => {
                setUsers(data.users.data)
                //Tämä on vähän turha
                infoToast()
            })
            .catch(err => err.response.status === 500 ? infoToast(0) : '')
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="App">
            <Stack>
                <Stack align={"center"}>
                    <Heading fontSize={"4xl"}>Käyttäjät</Heading>
                </Stack>
                <Box
                    p={8}
                >
                    <TableContainer>
                        <Table variant="striped">
                            <Thead>
                                <Tr>
                                    <Th>Nimi</Th>
                                    <Th>Sähköposti</Th>
                                    <Th>Osoite</Th>
                                </Tr>
                            </Thead>
                            <Tbody>
                                {users.map((user, index) =>
                                    <Tr key={index}>
                                        <Td>{user.name}</Td>
                                        <Td>{user.email}</Td>
                                        <Td>{user.address}</Td>
                                    </Tr>
                                )
                                }
                            </Tbody>
                        </Table>
                    </TableContainer>

                </Box>
            </Stack>
        </div>
    )

    /*  <ul> {users.map((user, index) => <li key={index}> {user.name} {user.email} {user.address}</li>)} </ul>) */
}

export default Users;

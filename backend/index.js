const express = require("express");
const app = express();
const port = 5000;


/**
 * Tässä tuli vähän vedettyä mutkia suoriksi
 */

const data = {
  code: "success",
  data: [
    {
      address: "Kehittäjäkatu 123",
      email: "erkki.esimerkki@kamk.fi",
      name: "Erkki Esimerkki"
    },
    {
      address: "Kehittäjäkatu 123",
      email: "kettu.kehittaja@kamk.fi",
      name: "Kettu Kehittäjä"
    },
    {
      address: "Kehittäjäkatu 123",
      email: "pete.koodari@kamk.fi",
      name: "Pete Koodari"
    },
    {
      address: "Kehittäjäkatu 123",
      email: "teppo.testaaja@kamk.fi",
      name: "Teppo Testaaja"
    },
    {
      address: "Kehittäjäkatu 123",
      email: "pertti@kamk.fi",
      name: "Pertti Projektipäälikkö"
    }
  ]
}

app.get('/api/users', (req, res) => {
  res.json({
    users: data
  })
})

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
})
